package main

import (
	"context"
	"encoding/base64"
	"fmt"
	"crypto/md5"
	"io"
	"os"
	"path/filepath"

	//"github.com/aws/aws-sdk-go-v2/aws"
	//"github.com/aws/aws-sdk-go-v2/aws/endpoints"
	"github.com/aws/aws-sdk-go-v2/aws/external"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

type SyncCheck struct {
	LocalMd5 []byte
	LocalETag *string
	CloudETag *string
}

var m map[string]*SyncCheck

func main() {
	if len(os.Args) < 3 {
		fmt.Fprintf(os.Stderr, "number of arguments is less than 2\n")
		fmt.Printf("Usage: %s <local dir> <bucket name>\n", os.Args[0])
		os.Exit(1)
	}
	fromDir := os.Args[1]
	toBucket := os.Args[2]
	m = make(map[string]*SyncCheck)

	// Cloud
	cfg, err := external.LoadDefaultAWSConfig()
	if err != nil {
		panic("unable to load SDK config, " + err.Error())
	}
	//cfg.Region = endpoints.ApNortheast2RegionID
	svc := s3.New(cfg)
	req := svc.ListObjectsV2Request(&s3.ListObjectsV2Input{
		Bucket: &toBucket,
	})
	resp, err := req.Send(context.Background())
	if err != nil {
		panic("failed to list objects, " + err.Error())
	}
	for _, obj := range resp.Contents {
		m[*obj.Key] = &SyncCheck{
			CloudETag: obj.ETag,
			LocalETag: nil,
		}
	}

	// Local
	err = os.Chdir(fromDir)
	if err != nil {
		panic("failed to chdir, " + err.Error())
	}
	err = filepath.Walk(".", local_path_walk)
	if err != nil {
		panic("failed to list files, " + err.Error())
	}

	// Sync
	for key, element := range m {
		if element.LocalETag == nil {
			err = delete_object(svc, &toBucket, &key)
			if err != nil {
				panic("failed to delete object, " + err.Error())
			}
		} else if element.CloudETag == nil ||
			*element.LocalETag != *element.CloudETag {
			err = put_object(svc, &toBucket, &key, element)
			if err != nil {
				panic("failed to put object, " + err.Error())
			}
		}
	}
}

func delete_object(svc *s3.Client, bucket *string, key *string) error {
	req := svc.DeleteObjectRequest(&s3.DeleteObjectInput{
		Bucket: bucket,
		Key: key,
	})
	_, err := req.Send(context.Background())
	fmt.Println("Delete Object:", *key)
	return err
}

func put_object(svc *s3.Client, bucket *string, key *string,
	        sync_check *SyncCheck) error {
	f, err := os.Open(*key)
	if err != nil {
		panic("failed to open file, " + err.Error())
	}
	defer f.Close()

	md5_base64 := base64.StdEncoding.EncodeToString(sync_check.LocalMd5)
	req := svc.PutObjectRequest(&s3.PutObjectInput{
		Bucket: bucket,
		Key: key,
		Body: f,
		ContentMD5: &md5_base64,
	})
	_, err = req.Send(context.Background())
	fmt.Println("Put Object:", *key, "MD5:", *sync_check.LocalETag)
	return err
}

func local_path_walk(path string, info os.FileInfo, err error) error {
	if err != nil {
		return err
	}
	if ! info.IsDir() {
		element, ok := m[path]
		if ok {
			element.LocalMd5, element.LocalETag = md5sum(path)
		} else {
			md5, etag := md5sum(path)
			m[path] = &SyncCheck{
				LocalMd5: md5,
				LocalETag: etag,
				CloudETag: nil,
			}
		}
	}
	return nil
}

func md5sum(path string) ([]byte,*string) {
	f, err := os.Open(path)
	if err != nil {
		panic("failed to open file, " + err.Error())
	}
	defer f.Close()

	h := md5.New()
	if _, err := io.Copy(h, f); err != nil {
		panic("failed to copy file to hash, " + err.Error())
	}

	sum := h.Sum(nil)
	str := fmt.Sprintf("\"%x\"", sum)
	return sum, &str
}
